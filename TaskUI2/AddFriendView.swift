//
//  AddFriendView.swift
//  TaskUI2
//
//  Created by Евгений Кацер on 05.05.2023.
//

import UIKit

struct WeakStorage<T> where T: AnyObject {
    weak var value: T?
}

class AddFriendView: UIViewController {
    weak var displayDelegate: FriendDisplayerDelegate!
    weak var checkDelegate: FriendsVMDelegate!
    
    @IBOutlet weak var firstNameTF: UITextField!
    @IBOutlet weak var lastNameTF: UITextField!
    @IBOutlet weak var ageTF: UITextField!
    @IBOutlet weak var sexTF: UITextField!
    @IBOutlet weak var placeOfWorkStudyTF: UITextField!
    @IBOutlet weak var phoneNumberTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    
    @IBOutlet weak var firstNameWarn: UILabel!
    @IBOutlet weak var lastNameWarn: UILabel!
    @IBOutlet weak var ageWarn: UILabel!
    @IBOutlet weak var sexWarn: UILabel!
    @IBOutlet weak var placeOfWorkOrStudyWarn: UILabel!
    @IBOutlet weak var phoneNumberWarn: UILabel!
    @IBOutlet weak var emailWarn: UILabel!
    
    @IBOutlet weak var submitButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lastNameWarn.isHidden = true
        placeOfWorkOrStudyWarn.isHidden = true
        emailWarn.isHidden = true
        
        submitButton.isEnabled = false
    }
    
    
    @IBAction func firstNameChanged(_ sender: Any) {
        unniversalChecker(text: firstNameTF.text, handler: invalidFirstName,
                          warnLabel: firstNameWarn)
    }
    
    @IBAction func lastNameChanged(_ sender: Any) {
        unniversalChecker(text: lastNameTF.text, handler: invalidLastName,
                          warnLabel: lastNameWarn)
    }
    
    @IBAction func ageChanged(_ sender: Any) {
        unniversalChecker(text: ageTF.text, handler: invalidAge,
                          warnLabel: ageWarn)
    }
    
    @IBAction func sexChanged(_ sender: Any) {
        unniversalChecker(text: sexTF.text, handler: invalidSex,
                          warnLabel: sexWarn)
    }
    
    @IBAction func placeOfWorkOrStudyChanged(_ sender: Any) {
        unniversalChecker(text: placeOfWorkStudyTF.text,
                          handler: invalidPlaceOfWorkOrStudy,
                          warnLabel: sexWarn)
    }
    
    @IBAction func phoneNumberChanged(_ sender: Any) {
        unniversalChecker(text: phoneNumberTF.text, handler: invalidPhoneNumber,
                          warnLabel: phoneNumberWarn)
    }
    
    @IBAction func emailChanged(_ sender: Any) {
        unniversalChecker(text: emailTF.text, handler: invalidEmail,
                          warnLabel: emailWarn)
    }
    
    func unniversalChecker(text: String?, handler: (String?) -> String?, warnLabel: UILabel) {
        if let errorMsg = handler(text) {
            warnLabel.text = errorMsg
            warnLabel.isHidden = false
        } else {
            warnLabel.isHidden = true
            checkForValidForm()
        }
    }
    
    func checkForValidForm() {
        if firstNameWarn.isHidden && ageWarn.isHidden &&
                sexWarn.isHidden && phoneNumberWarn.isHidden {
            submitButton.isEnabled = true
        } else {
            submitButton.isEnabled = false
        }
    }
    
    func invalidFirstName(_ value: String?) -> String?  {
        if checkDelegate.checkFirstName(value) {
            return nil
        } else {
            return "Name must contain only letters!"
        }
    }
    
    func invalidLastName(_ value: String?) -> String?  {
        let lastName = value?.isEmpty == true ? nil : value
        
        if checkDelegate.checkLastName(lastName) {
            return nil
        } else {
            return "Name must contain only letters!"
        }
    }
    
    func invalidAge(_ value: String?) -> String?  {
        if checkDelegate.checkAge(value) {
            return nil
        } else {
            return "Age must be positive number!"
        }
    }
    
    func invalidSex(_ value: String?) -> String?  {
        if checkDelegate.checkSex(value) {
            return nil
        } else {
            return "Sex must be 'Male' or 'Female'!"
        }
    }
    
    func invalidPlaceOfWorkOrStudy(_ value: String?) -> String?  {
        let placeOfWork = value?.isEmpty == true ? nil : value
        
        if checkDelegate.checkPlaceOfWorkOrStudy(placeOfWork) {
            return nil
        } else {
            return "There is a typo!"
        }
    }
    
    func invalidPhoneNumber(_ value: String?) -> String? {
        if checkDelegate.checkPhoneNumber(value) {
            return nil
        } else {
            return "Number must be in russian 11 digit format!"
        }
    }
    
    func invalidEmail(_ value: String?) -> String? {
        let email = value?.isEmpty == true ? nil : value
        
        if checkDelegate.checkEmail(email) {
            return nil
        } else {
            return "Invalid email!"
        }
    }
    
    @IBAction func didAddButtonPressed(_ sender: Any) {
        let allVals = [
            firstNameTF.text, lastNameTF.text, ageTF.text, sexTF.text,
            placeOfWorkStudyTF.text, phoneNumberTF.text, emailTF.text
        ]
        
        if displayDelegate.tryAddNewFriend(allVals) {
            dismiss(animated: true)
        }
    }
}
