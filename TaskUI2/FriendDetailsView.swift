//
//  FriendDetailsView.swift
//  TaskUI2
//
//  Created by Евгений Кацер on 05.05.2023.
//

import UIKit

class FriendDetailsView: UIViewController, UITableViewDelegate, UITableViewDataSource {
    private var friendInfo = [Record]()
    
    @IBOutlet weak var table: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.delegate = self
        table.dataSource = self
        
        table.registerCell(CustomOutputCell.self)
    }
    
    func storeFriendToShow(_ friend: Friend) {
        friendInfo = friend.getInfo()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        friendInfo.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: CustomOutputCell.getIdentifier(),
            for: indexPath
        ) as! CustomOutputCell
        
        
        let record = friendInfo[indexPath.row]
        cell.configure(textLeft: record.description, textRight: record.value ?? "-",
                       accessoryType: .none, selection: .none)
        
        return cell
    }
}
