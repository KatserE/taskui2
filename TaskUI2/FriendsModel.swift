//
//  DataModel.swift
//  TaskUI2
//
//  Created by Евгений Кацер on 05.05.2023.
//

protocol FriendsModelDelegate {
    func checkFirstName(_ value: String?) -> Bool
    func checkLastName(_ value: String?) -> Bool
    func checkAge(_ value: String?) -> Bool
    func checkSex(_ value: String?) -> Bool
    func checkPlaceOfWorkOrStudy(_ value: String?) -> Bool
    func checkPhoneNumber(_ value: String?) -> Bool
    func checkEmail(_ value: String?) -> Bool
    
    func getNumOfFriends() -> Int
    func getFriend(by index: Int) -> Friend?
    
    mutating func tryAddFriend(_ newFriend: Friend) -> Bool
    mutating func tryAddFriend(_ newFriend: [String?]) -> Bool
}

struct FriendsModel : FriendsModelDelegate {
    static let namesRegex = /[a-zA-z]+/
    static let ageRegex = /\d+/
    static let sexRegex = /(Male|Female)/
    static let mailRegex = /.+@.+\..+/
    // Checks only russian phone numbers
    static let phoneRegex = /^(\+7|8)[-\s]?\(?\d{3}\)?[-\s]?\d{3}[-\s]?\d{2}[-\s]?\d{2}$/
    
    private var friends = [Friend]()
    
    func checkFirstName(_ value: String?) -> Bool {
        guard let value else { return false }
        return value.contains(FriendsModel.namesRegex)
    }
    
    func checkLastName(_ value: String?) -> Bool {
        guard let value else { return true }
        return value.contains(FriendsModel.namesRegex)
    }
    
    func checkAge(_ value: String?) -> Bool {
        guard let value else { return false }
        guard value.contains(FriendsModel.ageRegex) else { return false }
        guard let _ = UInt8(value) else { return false }
        
        return true
    }
    
    func checkSex(_ value: String?) -> Bool {
        guard let value else { return false }
        return value.contains(FriendsModel.sexRegex)
    }
    
    func checkPlaceOfWorkOrStudy(_ value: String?) -> Bool {
        return true
    }
    
    func checkPhoneNumber(_ value: String?) -> Bool {
        guard let value else { return false }
        return value.contains(FriendsModel.phoneRegex)
    }
    
    func checkEmail(_ value: String?) -> Bool {
        guard let value else { return true }
        return value.contains(FriendsModel.mailRegex)
    }
    
    func getNumOfFriends() -> Int {
        friends.count
    }
    
    func getFriend(by index: Int) -> Friend? {
        guard index < getNumOfFriends() else {
            return nil
        }
        
        return friends[index]
    }
    
    mutating func tryAddFriend(_ newFriend: Friend) -> Bool {
        let firstNameCheck = newFriend.firstName.contains(FriendsModel.namesRegex)
        let lastNameCheck = newFriend.lastName?.contains(FriendsModel.namesRegex) ?? true
        let phoneNumberCheck = newFriend.phoneNumber.contains(FriendsModel.phoneRegex)
        let emailCheck = newFriend.email?.contains(FriendsModel.mailRegex) ?? true
        
        guard firstNameCheck && lastNameCheck && phoneNumberCheck && emailCheck else {
            return false
        }
        
        friends.append(newFriend)
        return true
    }
    
    mutating func tryAddFriend(_ newFriend: [String?]) -> Bool {
        let fields = newFriend.map { $0?.isEmpty == true ? nil : $0 }
        let newFriend = Friend.create(from: fields)
        guard let newFriend else {
            return false
        }
        
        return tryAddFriend(newFriend)
    }
}
