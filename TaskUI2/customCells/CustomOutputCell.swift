//
//  CustomTableViewCell.swift
//  TaskUI2
//
//  Created by Евгений Кацер on 05.05.2023.
//

import UIKit

class CustomOutputCell: UITableViewCell, CustomCell {
    @IBOutlet private weak var leftLabel: UILabel!
    @IBOutlet private weak var rightLabel: UILabel!
    
    func configure(textLeft: String, textRight: String,
                   accessoryType: AccessoryType, selection: SelectionStyle) {
        leftLabel.text = textLeft
        rightLabel.text = textRight
        
        self.accessoryType = accessoryType
        self.selectionStyle = selection
    }
}
