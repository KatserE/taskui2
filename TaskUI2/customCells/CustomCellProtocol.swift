//
//  CustomCellProtocol.swift
//  TaskUI2
//
//  Created by Евгений Кацер on 05.05.2023.
//

import UIKit

protocol CustomCell {
    static func getIdentifier() -> String
    static func getNib() -> UINib
}

extension CustomCell {
    static func getNib() -> UINib {
        UINib(nibName: String(describing: self), bundle: nil)
    }
    
    static func getIdentifier() -> String {
        String(describing: self)
    }
}
