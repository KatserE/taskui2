//
//  Friend.swift
//  TaskUI2
//
//  Created by Евгений Кацер on 05.05.2023.
//

enum Sex : String {
    case Male, Female
}

struct Record {
    let description: String
    let value: String?
}

struct Friend {
    let firstName: String
    let lastName: String?
    let age: UInt8
    let sex: Sex
    let placeOfWorkOrStudy: String?
    let phoneNumber: String
    let email: String?
    
    func getInfo() -> [Record] {
        let descriptions = Friend.getFieldsDescriptions()
        let vals = [
            firstName, lastName, String(age), sex.rawValue,
            placeOfWorkOrStudy, phoneNumber, email
        ]
        
        return zip(descriptions, vals).map { Record(description: $0.0, value: $0.1) }
    }
    
    static func getFieldsDescriptions() -> [String] {
        [
            "First name", "Last name", "Age", "Sex",
            "Place of work or study", "Phone number", "Email"
        ]
    }
    
    static func create(from fields: [String?]) -> Friend? {
        guard fields.count == getFieldsDescriptions().count else {
            return nil
        }
        
        let firstName = fields[0]
        let lastName = fields[1]
        let age = fields[2] != nil ? UInt8(fields[2]!) : nil
        let sex = fields[3] != nil ? Sex(rawValue: fields[3]!) : nil
        let placeOfWorkOrStudy = fields[4]
        let phoneNumber = fields[5]
        let email = fields[6]
        
        guard let firstName, let age, let sex, let phoneNumber else {
            return nil
        }
        
        return Friend(
            firstName: firstName, lastName: lastName, age: age, sex: sex,
            placeOfWorkOrStudy: placeOfWorkOrStudy, phoneNumber: phoneNumber,
            email: email
        )
    }
}
