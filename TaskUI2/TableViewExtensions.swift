//
//  TableViewExtensions.swift
//  TaskUI2
//
//  Created by Евгений Кацер on 05.05.2023.
//

import UIKit

extension UITableView {
    func registerCell(_ cell: CustomCell.Type) {
        register(cell.getNib(), forCellReuseIdentifier: cell.getIdentifier())
    }
}
