//
//  ViewController.swift
//  TaskUI2
//
//  Created by Евгений Кацер on 04.05.2023.
//

import UIKit

protocol FriendDisplayerDelegate : AnyObject {
    func tryAddNewFriend(_ newFriend: [String?]) -> Bool
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource,
                      FriendDisplayerDelegate {
    private static let detailsID = "friendDetailInfo"
    private static let addFriendID = "addNewFriend"
    
    // I don't really like the moment, that in this place view knows all not only
    // about VM but also about model. At the moment I don't know how to make it better
    private var friendsVM: FriendsVMDelegate = FriendsViewModel(delegate: FriendsModel())

    @IBOutlet weak var table: UITableView!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        table.delegate = self
        table.dataSource = self
        
        table.registerCell(CustomOutputCell.self)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        friendsVM.getNumOfRows()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(
            withIdentifier: CustomOutputCell.getIdentifier(),
            for: indexPath
        ) as! CustomOutputCell
        
        let friend = friendsVM.getFriend(by: indexPath)
        if let friend {
            cell.configure(textLeft: friend.firstName, textRight: String(friend.age),
                           accessoryType: .disclosureIndicator, selection: .none)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let friend = friendsVM.getFriend(by: indexPath)!
        performSegue(withIdentifier: ViewController.detailsID, sender: friend)
    }
    
    func tryAddNewFriend(_ newFriend: [String?]) -> Bool {
        if friendsVM.tryAddFriend(newFriend) {
            table.reloadData()
            return true
        } else {
            return false
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == ViewController.detailsID, let friend = sender as? Friend {
            let destination = segue.destination as! FriendDetailsView
            destination.storeFriendToShow(friend)
        } else if segue.identifier == ViewController.addFriendID {
            let destination = segue.destination as! AddFriendView
            destination.displayDelegate = self
            destination.checkDelegate = friendsVM
        }
    }
    
    @IBAction func didAddButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: ViewController.addFriendID, sender: nil)
    }
}
