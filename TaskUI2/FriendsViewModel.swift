//
//  FriendViewModel.swift
//  TaskUI2
//
//  Created by Евгений Кацер on 05.05.2023.
//

import UIKit

protocol FriendsVMDelegate : AnyObject {
    func checkFirstName(_ value: String?) -> Bool
    func checkLastName(_ value: String?) -> Bool
    func checkAge(_ value: String?) -> Bool
    func checkSex(_ value: String?) -> Bool
    func checkPlaceOfWorkOrStudy(_ value: String?) -> Bool
    func checkPhoneNumber(_ value: String?) -> Bool
    func checkEmail(_ value: String?) -> Bool
    
    func getNumOfRows() -> Int
    func getFriend(by index: IndexPath) -> Friend?
    
    func tryAddFriend(_ newFriend: Friend) -> Bool
    func tryAddFriend(_ newFriend: [String?]) -> Bool
}

class FriendsViewModel : FriendsVMDelegate {
    private var delegate: FriendsModelDelegate
    
    init(delegate: FriendsModelDelegate) {
        self.delegate = delegate
    }
    
    func checkFirstName(_ value: String?) -> Bool {
        delegate.checkFirstName(value)
    }
    
    func checkLastName(_ value: String?) -> Bool {
        delegate.checkLastName(value)
    }
    
    func checkAge(_ value: String?) -> Bool {
        delegate.checkAge(value)
    }
    
    func checkSex(_ value: String?) -> Bool {
        delegate.checkSex(value)
    }
    
    func checkPlaceOfWorkOrStudy(_ value: String?) -> Bool {
        delegate.checkPlaceOfWorkOrStudy(value)
    }
    
    func checkPhoneNumber(_ value: String?) -> Bool {
        delegate.checkPhoneNumber(value)
    }
    
    func checkEmail(_ value: String?) -> Bool {
        delegate.checkEmail(value)
    }
    
    func getNumOfRows() -> Int {
        delegate.getNumOfFriends()
    }
    
    func getFriend(by index: IndexPath) -> Friend? {
        delegate.getFriend(by: index.row)
    }
    
    func tryAddFriend(_ newFriend: Friend) -> Bool {
        delegate.tryAddFriend(newFriend)
    }
    
    func tryAddFriend(_ newFriend: [String?]) -> Bool {
        delegate.tryAddFriend(newFriend)
    }
}
